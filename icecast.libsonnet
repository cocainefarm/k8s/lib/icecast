{
  _config:: {
    icecast: {
      name: "icecast",
      image: {
        repo: "kube.cat/cocainefarm/icecast",
        tag: "2.4.4"
      },
      config: {
        location: "Earth",
        admin: "admin@example.com",
        hostname: "example.com",
        port: "8000",
        bind: "0.0.0.0",
        log_level: "2",
        source_password: "",
        relay_password: "",
        admin_user: "admin",
        admin_password: "",
        limit_clients: "100",
        limit_sources: "2",
        limit_queue_size: "524288",
        limit_client_timeout: "30",
        limit_header_timeout: "15",
        limit_source_timeout: "10",
        limit_burst_on_connect: "1",
        limit_burst_size: "65535",
      }
    },
  },

  local k = import "ksonnet-util/kausal.libsonnet",
  local statefulset = k.apps.v1.statefulSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,

  local withEnv(name, value) = container.withEnv(
    env.new(name=name, value=value)),
  
  icecast: {
    statefulset: statefulset.new(
      name=$._config.icecast.name
      , replicas=1
      , containers=[
        container.new(
          "icecast"
          , $._config.icecast.image.repo + ":" + $._config.icecast.image.tag
        ) + container.withPorts([port.new("icecast", std.parseInt($._config.icecast.config.port))])
        + container.withEnvFrom(k.core.v1.envFromSource.secretRef.withName($.icecast.secret.metadata.name))
        + container.withEnvMap({
          "ICECAST_LOCATION": $._config.icecast.config.location,
          "ICECAST_ADMIN": $._config.icecast.config.admin,
          "ICECAST_HOSTNAME": $._config.icecast.config.hostname,
          "ICECAST_PORT": $._config.icecast.config.port,
          "ICECAST_BIND": $._config.icecast.config.bind,
          "ICECAST_LOG_LEVEL": $._config.icecast.config.log_level,
          "ICECAST_LIMIT_CLIENTS": $._config.icecast.config.limit_clients,
          "ICECAST_LIMIT_SOURCES": $._config.icecast.config.limit_sources,
          "ICECAST_LIMIT_QUEUE_SIZE": $._config.icecast.config.limit_queue_size,
          "ICECAST_LIMIT_CLIENT_TIMEOUT": $._config.icecast.config.limit_client_timeout,
          "ICECAST_LIMIT_HEADER_TIMEOUT": $._config.icecast.config.limit_header_timeout,
          "ICECAST_LIMIT_SOURCE_TIMEOUT": $._config.icecast.config.limit_source_timeout,
          "ICECAST_LIMIT_BURST_ON_CONNECT": $._config.icecast.config.limit_burst_on_connect,
          "ICECAST_LIMIT_BURST_SIZE": $._config.icecast.config.limit_burst_size,
        })
      ]
    )
    + statefulset.spec.withServiceName($.icecast.service.metadata.name),
    service: k.util.serviceFor(self.statefulset) + service.spec.withClusterIP("None"),
    secret: k.core.v1.secret.new(name="%s-credentials" % $._config.icecast.name, data={}, type='Opaque')
      + k.core.v1.secret.withStringData({
        "ICECAST_ADMIN_USER": $._config.icecast.config.admin_user,
        "ICECAST_ADMIN_PASSWORD": $._config.icecast.config.admin_password,
        "ICECAST_SOURCE_PASSWORD": $._config.icecast.config.source_password,
        "ICECAST_RELAY_PASSWORD": $._config.icecast.config.relay_password,
      })
  }
}

